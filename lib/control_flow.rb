# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |ch|
    str.delete!(ch) if ch == ch.downcase
  end
  str

end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.odd?
    return str[str.length/2.floor]
  elsif str.length.even?
    return str[str.length/2-1] + str[str.length/2]
    end
end



# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  counter = 0
  str.each_char do |ch|
    counter += 1 if VOWELS.include?(ch)
  end
  counter
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)

end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  new_str = ""
  arr.each do |el|
    if arr.last == el
      new_str << el
    else
    new_str << el + separator
    end
  end
  new_str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_str = ""
  str.each_char.with_index do |ch, idx|
    new_str << ch.upcase if idx.odd?
    new_str << ch.downcase if idx.even?
  end
  new_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  new_arr =[ ]
  str.split.each do |el|
    if el.length >= 5
      new_arr << el.reverse
    else
      new_arr << el
    end
  end
  new_arr.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  new_arr = [ ]
  (1..n).each do |num|
    if num % 3 == 0 && num % 5 == 0
      new_arr << "fizzbuzz"
    elsif num % 3 == 0
      new_arr << "fizz"
    elsif num % 5 == 0
      new_arr << "buzz"
    else
      new_arr << num
    end
  end
  new_arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse

end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  (2..num/2).each do |factor|
    return false if num % factor == 0
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  new_arr = [ ]
  (1..num).each do |factor|
    new_arr << factor if num % factor == 0
  end
  new_arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  new_arr = [ ]
  (2..num).each do |factor|
    if prime?(factor) == true && num % factor == 0
      new_arr << factor
    end
  end
  new_arr
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length

end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even_arr = [ ]
  odd_arr = [ ]
  arr.each do |el|
    even_arr << el if el.even?
    odd_arr << el if el.odd?
  end
  return even_arr.first if even_arr.length == 1
  return odd_arr.first if odd_arr.length == 1
end
